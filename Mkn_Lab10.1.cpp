#include <graphics.h> 
#include <iostream>
#include <math.h>
#include <iostream>

using namespace std;
int main()
{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	float a=5; 
	float V1;
	V1=pow(a/2,3);
	char V[20];
	sprintf(V, "%3.3f", V1);
	initwindow(600,400);
	
	ellipse(300,50,0,360,100,20);
	line(200,50,170,300);
	line(400,50,430,300);
	line(200,50,400,50);
	ellipse(300,300,0,0,130,20);
	setlinestyle(1,1,1);
	line(300,50,300,300);
	line(170,300,430,300);
	line(200,50,300,300);
	line(300,300,400,50);
	ellipse(300,300,0,180,130,20);
	
	setlinestyle(0,1,1.5);
	
	outtextxy(425, 170, "a");
	outtextxy(410, 280, "b");
	outtextxy(180, 335, "V=(a)^2*h*PI = ");
	outtextxy(310, 335, V);
	
	getch();
	system("pause");
	closegraph();
	return 0; 
}
