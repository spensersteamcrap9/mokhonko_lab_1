#include <time.h>
#include <iostream>
#include <windows.h>
#include <fstream>

using namespace std;

void pv(int *v, int nv, int mv)
{
	int i, j;
	for(i = 0; i < nv; i ++)
	{
		for(j = 0; j < mv; j ++)
		{
			printf("%5d", *(v+i*mv+j));
		}
		cout<<"\n";
	}
}

void gv(int *v, int nv, int mv)
{
	int i, j;
	for (i = 0; i < nv; i ++)
	{
		for (j = 0; j < mv; j ++)
		{
			*(v+i*mv+j)=rand()%100-50;
		}
	}
}

void mv1(int *v, int nv, int mv, int *minm, int *im, int *jm)
{
	int i, j, mn;
	mn = *v;
	*im = 0;
	*jm = 0;
	for(i = 0; i < nv; i ++)
	{
		for(j = 0; j < nv ; j ++)
		{
			if(j == i)
			{
				if(mn > *(v+i*nv+i))
				{
					mn = *(v+i*nv+i);
					*im = i;
					*jm = j;
				}
			}
		}
	}
	*minm = mn;
}

void mv2(int *v, int nv, int mv, int *minc, int *ic, int *jc)
{
	int i, j, mn;
	mn = 0;
	*ic = 0;
	*jc = 0;
	for(i = 0; i < nv; i ++)
	{
		for(j = 0; j < nv ; j ++)
		{
			if(j == nv-i-1)
			{
				if(mn > *(v+i*nv+j))
				{
					mn = *(v+i*nv+j);
					*ic = i;
					*jc = j;
				}
			}
		}
	}
	*minc = mn;
}

int main()

{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int n1, m1;
	int n2, m2;
	int im, jm, ic, jc, ii, ji, io, jo;
	int minm, minc, mini, mino;
	fstream f1; //f1 - ������� ����
	FILE *f2; //f2 - �������� ����
	char sf1[40], sf2[40];// ����� ��� ������� ����� �����
	printf("������ ��'� �������� �����: ");
	gets(sf1);
	f1.open(sf1); //��������� ���� ��� ���������� � ���������� �����
	if(!f1)
	{
	printf("������� ������� ��� ������� ����� \n");
	return 0;
	}
	f1 >> n1;
	f1 >> m1;
	f1 >> n2;
	f1 >> m2;
	int G[n1][m1], W[n2][m2];
	printf("������ ��'� ����� ��� ����������: ");
	gets(sf2);
	f2=freopen(sf2, "w+", stdout); //��������� �� ��������������� ���� ��������� � ������� ����
	//���� ������� ������� �� ������������� ���� ���������, �� ��������� ���� ������������ � ������� ���� � �� ���������� �� �����
	if(f2== NULL)
	{
	printf("������� ������� ��� ������� ����� \n");
	return 0;
	}
	gv(&G[0][0], n1, m1);
	gv(&W[0][0], n2, m2);
	system("cls");
	printf(" ������ ������� G(%d,%d):\n", n1, m1);
	pv(&G[0][0], n1, m1);
	printf(" ������ ������� W(%d,%d):\n", n2, m2);
	pv(&W[0][0], n2, m2);
	
	mv1(&G[0][0], n1, m1, &minm, &im, &jm);
	printf(" ̳��������� ������� ������� ������� ������� G(%d,%d) = %d �� ���� ������ (%d,%d) \n", n1, m1, minm, im, jm);
	mv2(&G[0][0], n1, m1, &minc, &ic, &jc);
	printf(" ̳��������� ������� ������ ������� ������� G(%d,%d) = %d �� ���� ������ (%d,%d) \n", n1, m1, minc, ic, jc);
	mv1(&W[0][0], n2, m2, &mini, &ii, &ji);
	printf(" ̳��������� ������� ������� ������� ������� W(%d,%d) = %d �� ���� ������ (%d,%d) \n", n2, m2, mini, ii, ji);
	mv2(&W[0][0], n2, m2, &mino, &io, &jo);
	printf(" ̳��������� ������� ������ ������� ������� W(%d,%d) = %d �� ���� ������ (%d,%d) \n", n2, m2, mino, io, jo);
	system("pause");
	return 0;	
}
