#include <time.h>
#include <iostream>
#include <windows.h>

using namespace std;

void pv(int *v, int nv, int mv)
{
	int i, j;
	for(i = 0; i < nv; i ++)
	{
		for(j = 0; j < mv; j ++)
		{
			printf("%4d", *(v+i*mv+j));
		}
		cout<<"\n";
	}
}

void gv(int *v, int nv, int mv)
{
	int i, j;
	for (i = 0; i < nv; i ++)
	{
		for (j = 0; j < mv; j ++)
		{
			*(v+i*mv+j) = rand()%100-50;
		}
	}
}

void sv(int *v, int nv, int mv)
{
	int i, j, is, js;
	js=-1;
	int newv[nv][nv];
	for(i = 0; i < nv; i ++)
	{
		for(j = 0; j < mv; j ++)
		{
			newv[i][j]=0;
		}
	}
	for(j = 0; j < mv; j ++)
	{
		js=-1;
		for(i = 0; i < nv; i ++)
    	{
			if(*(v+i*mv+j) < 0)
			{
				js++;
				newv[js][j] = *(v+i*mv+j);
			}
		}
		for(i = 0; i < nv; i ++)
		{
			if((*(v+i*mv+j) >= 0))
			{
				js++;
				newv[js][j] = *(v+i*mv+j);
			}
		}
    }
	for(i = 0; i < nv; i ++)
	{
		for(j = 0; j < mv; j ++)
		{
			*(v+i*mv+j)=newv[i][j];
		}
	}
}

int main()
{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int i, j, n, m;
	srand(time(NULL));
	cout<<" ������� ������� ����� ������� n = ";
	scanf("%d", &n);
	cout<<" ������� ������� ��������� ������� m = ";
	scanf("%d", &m);
	int A[n][m], B[n][m];
	printf("\n ������ ������� A(%d, %d):\n", n, m);
	gv(&A[0][0], n, m);
	pv(&A[0][0], n, m);
	sv(&A[0][0], n, m);
	printf("\n ����������� ������� A(%d, %d):\n", n, m);  
	pv(&A[0][0], n, m);
	system("pause");
	return 0;
}

