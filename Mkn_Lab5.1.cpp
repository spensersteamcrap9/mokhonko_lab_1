#include <fstream>
#include <time.h>
#include <iostream>
#include <windows.h>
#include <math.h>

using namespace std;

int cpv(double *v, int nv)
{
	int i;
	for(i=0; i<nv; i++)
	{
		v[i] = (rand()%100-50) / (10.0*(rand()%19+1));
		printf("%5.2f  ", v[i]);
	}
	cout<<"\n";
	return 0;
}

double minv(double *v, int nv)
{
	double mv;
	int i;
	mv = v[0];
	for(i=1; i<nv; i++)
	{
		if(mv>v[i])
		{
			mv=v[i];
		}
	}
	return mv;
}

double F1(double a, double b, double c, const double fD, double g)
{
	double F;
	F = (exp(a)) + fD * g * (b + c);
	return F;
}

int main()

{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	const double D=2.11;
	int i, ig, n1, n2, n3;
	srand(time(NULL));
	fstream f1; //f1 - ������� ����
	FILE *f2; //f2 - �������� ����
	char sf1[40], sf2[40];// ����� ��� ������� ����� �����
	printf("������ ��'� �������� �����: ");
	gets(sf1);
	f1.open(sf1); //��������� ���� ��� ���������� � ���������� �����
	if(!f1)
	{
	printf("������� ������� ��� ������� ����� \n");
	return 0;
	}
	f1 >> n1;
	f1 >> n2;
	f1 >> n3;
	f1 >> ig;
	printf("������ ��'� ����� ��� ����������: ");
	gets(sf2);
	f2=freopen(sf2, "w+", stdout); //��������� �� ��������������� ���� ��������� � ������� ����
	//���� ������� ������� �� ������������� ���� ���������, �� ��������� ���� ������������ � ������� ���� � �� ���������� �� �����
	if(f2== NULL)
	{
	printf("������� ������� ��� ������� ����� \n");
	return 0;
	}
	double A[n1], B[n2], C[n3];
	cout<<" ����������� ����� A["<<n1<<"] = ";
	cpv(A, n1);
	cout<<" ����������� ����� B["<<n2<<"] = ";
	cpv(B, n2);
	cout<<" ����������� ����� C["<<n3<<"] = ";
	cpv(C, n3);
	double F;
	F = F1(minv(A, n1), minv(B, n2), minv(C, n3), D, ig);
	printf(" Min ������ A = %5.2f\n", minv(A, n1));
	printf(" Min ������ B = %5.2f\n", minv(B, n2));
	printf(" Min ������ C = %5.2f\n", minv(C, n3));
	printf(" F = %7.5f\n", F);
	system("pause");
	return 0;
}

