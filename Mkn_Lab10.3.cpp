#include <windows.h> 
#include <graphics.h> 
#include <iostream> 
#include <ctime>
#include <cmath>
using namespace std;

void f1(int x,int y)
{
	setfillstyle(1,6);
	bar(0+x,0+y,40+x,200+y);
}

void f2(int x,int y, int j)
{
	setfillstyle(1,6);
	bar(0+x,0+y,x+j,30+y);
}

int main() 
{
int i=0;
initwindow(1200,700);

int poly[10];
poly[0] = 250;
poly[1] = 360;

poly[2] = 450;
poly[3] = 150;

poly[4] = 750;
poly[5] = 150;

poly[6] = 980;
poly[7] = 360;

poly[8] = poly[0];
poly[9] = poly[1];

while(true)
{
setfillstyle(1,11);
bar(0,0,1200,550);
setfillstyle(1,2);
bar(0,1200,1200,550);


Sleep(1000);

f1(300,350);
Sleep(500);

f1(450,350);
Sleep(500);

f1(700,350);
Sleep(500);

f1(900,350);
Sleep(500);

f2(300,350,180);
Sleep(500);

f2(700,350,210);
Sleep(500);

f2(300,350,510);
Sleep(500);

int i=0;
while(i<7)
{
	f2(300,520-i*25,180);
	Sleep(500);
	i++;
}

i=0;
while(i<7)
{
	f2(700,520-i*25,210);
	Sleep(500);
	i++;
}

i=0;
while(i<7)
{
	f2(300,520-i*25,510);
	Sleep(500);
	i++;
}

setfillstyle(1,7);
bar(570,550,630,430);
Sleep(500);

setfillstyle(1,3);
bar(400,500,500,430);
Sleep(500);

setfillstyle(1,3);
bar(700,500,800,430);
Sleep(500);

setfillstyle(1,8);
fillpoly(5,poly);

Sleep(2000);
clearviewport();
}
getch(); 
closegraph();
return 0; 
}
